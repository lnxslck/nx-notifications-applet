# NX Notifications Applet [![Build Status](https://travis-ci.org/nx-desktop/nx-notifications-applet.svg?branch=master)](https://travis-ci.org/nx-desktop/nx-notifications-applet)

This is the repository for the notifications center used in Nitrux.

![](https://i.imgur.com/zu5nhs6.png)

# Requirements
- Qt 5.8+.
- Qml.
- QtQuick.
- I18n 
- Plasma 5.8.4+.
- WindowSystem.
- KIO.

# Issues
If you find problems with the contents of this repository please create an issue.

©2018 Nitrux Latinoamericana S.C.
